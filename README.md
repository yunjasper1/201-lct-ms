# 201-LCT-MS

Programming assignments for 201-LCT-MS: Math for Computer Science course at Marianopolis College for the Winter 2019 semester.

**Assignment 1: Guessing Game** <br>
This program allows the user to play a number guessing game with the computer. The user guesses integers until they guess correctly, based on hints from the computer.
The computer can also guess the user's number. The user gives the computer hints by indicating whether a guess is too high or too low.


**Assignment 2: Quotient Remainder Theorem** <br>
This program builds a quotient function that performs the quotient and remainder when dividing two integers. The program contains both an iterative and recursive version of the quotient function with a menu to choose which function to use.


**Assignment 3: Euclidean Algorithm** <br>
This program contains a recursive,  verbal recursive, and iterative version of a function that performs the Euclidean Algorithm. It uses functions defined in previous assignments.


**Assignment 4: Factoring and Prime Numbers** <br>
This program contains functions that: <br>
    -determine whether an integer is prime or not <br>
    -list all the primes less than an input number <br>
    -returns the prime factorization of an input integer <br>

There is a menu for the user to choose what to do.


**Assignment 5: Sets** <br>
This program contains functions that: <br>
    -take in a list of integers and returns the power set as a list based on a proof by induction <br>
    -take in a list of integers and prints the power set in mathematics notation <br>
    -presents a menu to the user to create the set of integers and to print the power set <br>


**Final Project:** <br>
The final project consisted of multiple subprojects: <br>
-Setting up RSA encryption <br>
-Encoding messages using RSA <br>
-Decoding messages using RSA <br>
-Creating a class called Person which gives them a unique ID, Name, Balance, and RSA data<br>
-Creating a class called Transaction which tracks transactions between Person objects<br>
-Creating a menu to allow user input and choice of task to perform
