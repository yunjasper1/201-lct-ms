"""Jasper Yun - Student ID: 1731131
Exercise 2: Modular Arithmetic
This program is used to calculate the quotients and remainders of two integers given to the program by the user.
There is a menu option to choose different methods to calculate a mod n, or to exit the program."""

#Question 1:
"""This function takes an input of two integers, a and d, and divides them to find the
quotient and remainder. The division is performed by subtracting d from a until a < d. If a < 0,
then the division uses a + d until a > 0.
The output of the function is the quotient and remainder of the division given by q, r."""

def Quotient(a,d):
    r=a
    q=0
    while (r>=d): ##if r >= d, we can keep subtracting r - d until r < d, at which point we will be finished the division
        r=r-d
        q=q+1
    while (r<0): ##if r < 0, then we need to keep adding r + d until r > 0, at which point we will necessarily have 0 <= r < d, and we will be done dividing
        r=r+d
        q=q-1
    return q,r ##I chose not to return a tuple so that I could make the remainder (result of menu option 3) more salient



#Question 2:
"""This function takes an input of integers a, d, and q. It divides a and d to find the
quotient and remainder recursively. The input q is the quotient, which is set to zero initially.
If the input is a >= 0, then there are two cases:
    (1) a < d, then the function will return (q, a);
    (2) a >= d, then the function will call itself and perform the division using recursive subtraction.
If the input is a < 0, then there are also two cases:
    (1) a > d, then the function will return (q, a);
    (2) a <= d, then the function will call itself and perform the division using recursive addition.
The condition is that d > 0, since d != 0 and my function will not return the proper result if d < 0.
The outut is the quotient and remainder, given by q, a."""

def RecursiveQuotient(a,d, q=0):
    if a >= 0:##for positive integer inputs
        if a < d:
            return (q,a) ##I return a tuple here because I only need the recursive function for option 2 of the menu, which needs to output quotient and remainder, (q, r)
        else:
            return RecursiveQuotient(a - d, d, q + 1)
    elif a < 0: ##for negative integer inputs
        if a > 0: ##a>0 will only occur once a + d is performed enough times 
            return (q, a)
        else:
            return RecursiveQuotient(a + d, d, q - 1)



###Extra function that I defined to make question 3 slightly easier
"""This function is used to loop back the menu options. It technically does not have any parameters.
It provides the user with an option to return to the menu or exit the program based on user input.
Entering 1 calls the Menu() function. Inputting 2 exits the program."""

def Loop():
    print("")
    print("Alright! That was easy, wasn't it?")
    print("Here are two more options for you now:")
    ans = int(input("Enter 1 if you would like to return to the menu. Enter 2 if you would like to exit the program. "))
    while not ((ans==1) or (ans==2)): ##To prevent an input that is not 1 or 2
           ans = int(input("You must choose an option by entering 1 or 2. "))
    if (ans == 1):
        Menu()
    if (ans == 2):
        print("")
        print("Have a nice day! Goodbye!")



#Question 3:
"""This function is a menu to choose how divide two integers, a and d. It does not have any parameters, but to continue the program the user needs to enter a value for "choice", either 1, 2, 3, or 4. These choices will lead the program to using
either the standard quotient method defined in question 1, the recursive quotient defined in question 2, reducing
an integer into a modulo n, or exiting the program. This function combines the two functions above, and the choices commence
one of the above functions."""

def Menu():
    print("") #Some blank space
    print("Here are your menu options:")
    print("Enter 1 if you would like to divide two integers using the standard quotient.")
    print("Enter 2 if you would like to use the recursive quotient.")
    print("Enter 3 if you would like to reduce an integer a modulo n.")
    print("Enter 4 if you would like to exit the program.")
    choice = input("Please choose an option now. ")
    
    while not ((choice=="1") or (choice=="2") or (choice=="3") or (choice=="4")): ##To prevent the program from crashing if the input is not 1,2,3, or 4.
        choice = input("You must choose an option by inputting 1, 2, 3, or 4. ")
    choice = int(choice) ##change the input into an integer since I did not specify that d was an integer above so that I could properly deal with non-integer inputs for "choice" and prevent the program from crashing

    if (choice == 1): 
        print("")
        print("Great choice. Let's begin.")
        a = int(input("Please choose an integer for a. "))
        d = int(input("Please choose a nonzero integer for d to divide a. "))
        while (d == 0 or d<0): ##to prevent errors: (1) we cannot divide by 0; (2) my function will not run properly if d < 0; (3) the assignment instructions also indicate that d > 0
            if d == 0:
                d = int(input("Hey! You should know that we do not divide by zero. Please choose another integer for d: "))
            if d<0:
                d = int(input("Unfortunately, dividing by a negative number is not currently supported. Please choose another integer for d: "))
        q,r = Quotient(a,d)
        print("The quotient and remainder of", a,"and", d, "are given by (q,r) = (", q,",",r,").")
        Loop() ##to allow the user to loop back to the menu or quit

    if (choice == 2):
        print("")
        print("Ahh, a bold move that is, using recursion. I like it!")
        print("Let's begin with choosing the values.")
        a = int(input("Please choose an integer for a. "))
        d = int(input("Please choose a nonzero positive integer for d to divide a. "))
        while (d==0 or d<0): ##to prevent errors: (1) we cannot divide by zero; (2) the function will not run properly if d < 0; (3) the assignment only requires that a, d > 0
            if d==0:
                d = int(input("Hey! You should remember that we cannot divide by zero. Please choose a different integer for d. "))
            if d<0:
                d = int(input("Unfortunately, dividing by a negative integer is not currently supported by this program. Please choose a positive integer for d. "))
        q,r = RecursiveQuotient(a,d)
        print("The quotient and remainder of", a,"and", d, "are given by (q,r) = (", q,",",r,").")
        Loop()

    if (choice == 3):
        print("")
        print("Alright! Let me do the work for you.")
        a = int(input("Please choose the integer that you would like to reduce. "))
        d = int(input("Please choose the modulus you would like to reduce in. "))
        while (d==0 or d<0): ##to prevent errors: (1) we cannot divide by zero; (2) the function will not run properly if d < 0
            if d==0:
                d = int(input("Hey! You should remember that we cannot divide by zero. Please choose a different integer for d. "))
            if d<0:
                d = int(input("Unfortunately, negative moduli are not currently supported by this program. Please choose a positive integer for d. "))
        q,r = Quotient(a,d)
        print("The quotient and remainder of", a,"and", d, "are given by (q,r) = (", q,",",r,").")
        print("Therefore,", a, "mod", d, "reduces to", r,".")
        Loop()

    if (choice == 4):
        print("")
        print("Have a nice day! Goodbye!")



##Some text to introduce the program before the menu options; they are placed here rather than in the Loop() so that they do not keep appearing every time we loop
print("Hello! Welcome to my modular arithmetic calculator.")
print("This program will allow you to calculate the quotient and remainder of two values, a and d, when dividing a by d. You will choose a and d.")
print("You may choose two different methods to calculate the quotient, or reduce an integer a modulo n.")
print("") #provide a line break before the menu options
               
Menu() #to run the program
