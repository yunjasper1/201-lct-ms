"""Jasper Yun, Student ID: 1731131
Computer Exercise 3: Euclidean Algorithm
The purpose of this program is to use the Euclidean Algorithm to find the Greatest Common Factor of two integers, or the inverse
of an integer a modulo n using the Euclidean Algorithm."""

#note: in statements of print("%s some text here %s" % (x, y)), %s is the format operator

#Question 1:

def quotient(a,b): #from previous exercise
    """This function takes an input of two integers, a and d, and divides them to find the
        quotient and remainder. The division is performed by subtracting d from a until a < d. If a < 0,
        then the division uses a + d until a > 0.
        The output of the function is the quotient and remainder of the division given by q, r."""
    r=a
    q=0
    while (r>=b): #if r >= b, we can keep subtracting r - b until r < b, at which point we will be finished the division
        r=r-b
        q=q+1
    while (r<0): #if r < 0, then we need to keep adding r + b until r > 0, at which point we will necessarily have 0 <= r < b, and we will be done dividing
        r=r+b
        q=q-1
    return (q,r)


def Euclidean(a,b):
    """This is a recursive function that uses the Euclidean Algorithm to find the GCF of two integers. It takes in an input of two positive integers, a and b,
        and computes the following: d = gcd(a,b) = s(a) + t(b), where d, s, t (integers) are the outputs of the function.
        Note that by question statement, a,b are positive integers, so we do not need to consider negative inputs in any of the functions."""
    (q,r)=quotient(a,b)
    if r==0:
        return (b,0,1)
    else:
        (d1,s1,t1)=Euclidean(b,r) #d = gcd(a,b), and we have that 1 = sb + tr = 1 if r!=0
        d=d1
        s=t1
        t=s1-t1*q
        return (d,s,t)



#Question 2:

def EuclideanFull(a,b):
    """This function is a verbal version of Euclidean(a,b), performing the same operations as Euclidean(a,b), described above.
        It takes an input of two positive integers a and b.
        It prints each step of the divisions performed by quotient(a,b).
        The outputs are integers d,s,t, same as the outputs of Euclidean(a,b)."""
    (q,r)=quotient(a,b)
    if r==0:
        print("%s = %s(%s) + 0" % (a, q, b))
        print("Now we go back!")
        return (b,0,1)
    else:
        print("%s = %s(%s) + %s" % (a, q, b, r))
        (d1,s1,t1)=EuclideanFull(b,r)
        d=d1
        s=t1
        t=s1-t1*q
        print("%s = %s(%s) + %s(%s - %s(%s)) = %s(%s) + %s(%s)" % (d, s1, b, t1, a, q, b, t1, a, t ,b))
        return(d,s,t)




#Question 3:
#I did not put an option in the menu to run this function

def EuclideanNonRecursive(a,b):
    """This is a iterative version of Euclidean(a,b). It takes an input of two positive integers a, b. The outputs are integers d,s,t, where d = gcd(a,b) = s(a) + t(b)."""
    A = []
    rem = True
    while rem == True: #to keep the division going until r=0, i.e. the division is finished when the remainder is zero
        (q,r) = quotient(a,b)
        if r==0:
            d=b
            rem = False
        else:
            A.append(q) #adding q to the list A for later use
            (a,b) = (b,r)
            d = r
    s = 0                       #initialize value of s = 0, similar to what was done in Euclidean(a,b)
    t = 1                       #initialize value of t = 1
    for i in range(len(A)):     #to go through all of the quotients in the list A
        q = A.pop()             #let q = last value in list A, as described in the exercise instructions
        s1 = s                  #the values below are set using the formula found in Euclidean(a,b)
        t1 = t
        s = t1
        t = s1-t1*q             #same formula as we used in Euclidean(a,b)
    return (d,s,t)


#Question 4:

def Loop():
    #Function copied from last assignment to loop back to the menu until the user chooses to leave.
    """This function is used to loop back the menu options. It technically does not have any parameters.
    It provides the user with an option to return to the menu or exit the program based on user input.
    Entering 1 calls the Menu() function. Inputting 2 exits the program."""

    print("")
    print("Alright! That was easy, wasn't it?")
    print("Here are two more options for you now:")
    ans = int(input("Enter 1 if you would like to return to the menu. Enter 2 if you would like to exit the program. "))
    while not ((ans==1) or (ans==2)): ##To prevent an input that is not 1 or 2
        ans = int(input("You must choose an option by entering 1 or 2. "))
    if (ans == 1):
        Menu() #brings user back to the menu
    if (ans == 2):
        print("")
        print("Have a nice day! Goodbye!")


def Menu():
    """This function is a menu to choose different methods to find the GCD of two positive integers a, b.
It does not have any parameters, but to continue the program the user needs to enter a value for "choice", either 1, 2, 3, 4, or 5.
Choosing a value for "choice" will commence one of the above functions, allowing the user to find the GCD of two integers."""


    print("") #Some blank space
    print("Here are your menu options. Please enter a value depending on whether you would like to: ")
    print("1: find the GCF of two positive integers a and b. ")
    print("2: write the GCF of two positive integers a and b as a linear combination of a and b. ")
    print("3: find the GCF of two positive integers a and b and be shown all of the steps of the Euclidean Algorithm. ")
    print("4: find the inverse of two positive integers a and b. ")
    print("5: exit the program. ")
    choice = input("Please choose an option now. ")
    
    while not ((choice=="1") or (choice=="2") or (choice=="3") or (choice=="4") or (choice=="5")): #To prevent the program from crashing if the input is not 1,2,3,4, or 5.
        choice = input("You must choose an option by inputting 1, 2, 3, 4, or 5. ")
    choice = int(choice) #changes the input into an integer since I did not specify that "choice" was an integer above to deal with non-integer inputs for "choice" and prevent the program from crashing

    if (choice == 1): 
        print("")
        print("Great choice.")
        a = int(input("Please choose a positive integer for a. "))
        while a<0: #to prevent errors since I did not program for negative numbers; the assignment instructions also indicate that a,b > 0
            a = int(input("Please choose a positive integer for a. "))
        b = int(input("Please choose a positive non-zero integer for b. "))
        while (b==0 or b<0): #to prevent errors since I did not program for negative numbers the assignment instructions also indicate that a,b > 0
            if b<0:
                b = int(input("Please choose a positive integer for b. "))
            if b==0:
                b = int(input("We cannot divide by zero. Please choose a positive integer for b. "))
        d,s,t = Euclidean(a,b)
        print("The greatest common factor of %s and %s is %s." % (a, b, d)) #I used %s as placeholders to make it easier to read the code; this is used throughout the rest of the program
        Loop() #to allow the user to loop back to the menu or quit

    if (choice == 2):
        print("")
        print("Let's start with choosing the values.")
        a = int(input("Please choose a positive integer for a. "))
        while a<0: #to prevent errors
            a = int(input("Please choose a positive integer for a. "))
        b = int(input("Please choose a positive integer for b. "))
        while (b==0 or b<0): #to prevent errors
            if b<0:
                b = int(input("Please choose a positive integer for b. "))
            if b==0:
                b = int(input("We cannot divide by zero. Please choose a positive integer for b. "))
        d,s,t = Euclidean(a,b)
        print("Using the Euclidean Algorithm, the GCF of %s and %s is %s = (%s)%s + (%s)%s." % (a, b, d, s, a, t, b))
        print("Therefore, the GCF is %s." % (d))
        Loop()

    if (choice == 3):
        print("")
        print("Alright! Let me walk you through this.")
        a = int(input("Please choose a positive integer for a. "))
        while a<0: #to prevent errors
            a = int(input("Please choose a positive integer for a. "))
        b = int(input("Please choose a positive integer for b. "))
        while (b==0 or b<0): #to prevent errors
            if b<0:
                b = int(input("Please choose a positive integer for b. "))
            if b==0:
                b = int(input("We cannot divide by zero. Please choose a positive integer for b. "))
        d,s,t = EuclideanFull(a,b)
        print("")
        print("Voila! Beautiful, right?")
        Loop()

    if (choice == 4):
        print("")
        a = int(input("Please choose a positive integer for a. "))
        while a<0: #to prevent errors
            a = int(input("Please choose a positive integer for a. "))
        n = int(input("Please choose a positive nonzero integer for n, the modulus in which you would like to find the inverse of a. "))
        while (n==0 or n<0): #to prevent errors; also, we cannot divide by zero, i.e. we cannot have mod 0
            if n == 0:
                n = int(input("Hey! By definition of the modulus, n > 0. Please choose a positive integer for n. "))
            if n<0:
                n = int(input("Please choose a positive integer for n. "))
        d,s,t = Euclidean(a,n)
        print("Using the Euclidean Algorithm, GCF(%s,%s) = %s = (%s)%s + (%s)%s" % (a, n, d, s, a, t, n))
        if s<0:
            s=s+n
            print("If you like negative inverses, then the inverse of %s modulo %s is %s." % (a, n, s-n))
            print("If you like positive inverses, then %s is equivalent to %s modulo %s." % (s-n, s, n))
        if d != 1:
            print("%s and %s are not relatively prime since the GCF(%s,%s) is not 1." % (a, n, a, n))
            print("Therefore, there is no inverse for %s with respect to mod %s." % (a, n))
        else:
            print("Therefore, the inverse of %s modulo %s is %s." % (a, n, s))
        Loop()

    if (choice == 5):
        print("")
        print("Have a nice day! Goodbye!")



##Some text to introduce the program before the menu options; they are placed here rather than in the Loop() so that they do not keep appearing every time we loop
print("Hello! Welcome to my Greatest Common Factor Calculator!")
print("This program will allow you to calculate the GCF of two positive integers, a and b, in various methods. You will choose a and b.")
print("Unfortunately, at this time, we do not support values of 0 (zero) for b.")
print("(To run the non-recursive Euclidean Algorithm, run EuclideanNonRecursive(a,b), and choose your integers values for a and b; note that b cannot be zero since we cannot divide by zero.)")
print("Let's begin!")
print("") #provide a line break before the menu options
               
Menu() #to run the program
