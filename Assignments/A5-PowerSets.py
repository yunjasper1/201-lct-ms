"""Jasper Yun, Student ID: 1731131
Exercise 5: Power Sets
This program seeks to generate the power set of a list, A, inputted by the user."""



#From SetsInPython.py given to us:
#I modify them for more robustness


def Create():
    """#This function returns a set with the size and the elements chosen by the user. The elements need to (not necessarily) be integer."""
    Correct=False                   #commence the while loop below
    while (Correct==False):         #we do not want negative sizes of sets
        a = (input("Please choose a positive, nonzero integer for the size of your set. "))
        try:
            n=int(a)
            if n<0:
                Correct=False       #continues the while loop if n<0 since set size non-negative (but can be zero)
                print("Set sizes are always non-negative. \n")
            else:
                Correct=True        #breaks the while loop
        except ValueError:
            print("Hey! That was not an integer. \n")
            Correct=False           #keeps the while loop going
    A=[]
    for i in range(n):

        Correct=False               #commence the while loop below
        while (Correct==False):     #we want integers in our set
            NewElement = (input("What is the next integer in your set? "))
            try:
                n=int(NewElement)
                Correct=True        #breaks while loop
            except ValueError:
                print("Hey! That was not an integer. \n")
                Correct=False       #keeps the while loop going
                
        A.append(n)                 #add all new elements to A
        single(A)                   #remove repeated entries (function defined below)
    return A


def Print(A):
    """This function prints the set A using correct mathematics notation."""
    toprint="{" #To avoid having tons of linebreak, I build a string and print it all at once later on.
    for x in A:
        toprint+=str(x)
        toprint+=","
    if len(A)>0:
        toprint=toprint[:-1] #Removes the last character, which is an extra period, from the string.
    toprint+="}"
    return(toprint)






#Function I created to make remove repeating elements from sets inputted by the user:

def single(A):
    """This function takes an input of a list, A, and removes repeating elements A.
        It outputs A without its repeating elements"""
    for k in A:
        if A.count(k)>1:        #commences if the element repeats itself in the list
            A.remove(k)         #removes the repeating element in A
            single(A)           #runs the function again to ensure that all repeated elements are removed
    return A                    #outputs list without repeating elements


#Question 1:

def Power(A):
    """This function takes an input of a list, A, and outputs the Power Set of A as a list,
        which is the set of all subsets of A."""
    single(A)                   #remove repeating entries in the inputted list A
    if len(A) == 0:             #the only set with length zero is the empty set ---> this is the base case
        return [[]]             #creates the set of the empty set as the power set
    else:
        B=A.copy()              #duplicate A, which has k+1 elements
        z=B.pop()               #remove last element of list A so A now has k elements
        PB=Power(B)             #recursive step of induction giving us the Power Set of B (PB)
        PBC=Power(B)            #second copy of the Power Set of B
        for k in PB:
            L=k.copy()          #duplicate all elements already in Power Set of B
            L.append(z)         #add z to each of the elements, i.e. do the union of Power(B) and {z}
            PBC.append(L)       #PB already has all elements in Power Set of B without z, now add the elements with the z to get Power Set of A
        single(PBC)             #remove any extra entries in PBC
        return PBC              #print the power set of A


#Question 2:

def PrintPower(A):
    """This function takes an input of a list, A, and outputs the power set of A as a list in the correct mathematics notation."""
    Power(A)                    #creates the Power Set of A; the next code is from Print(A), which was given in SetsInPython.py
    toprint="{"                 #To avoid having tons of linebreak, I build a string and print it all at once later on.
    for x in Power(A):          #run Print(x) function on every element in the list of Power(A), i.e. the Power Set of A
        toprint+= Print(x)  
        toprint+=","
    toprint=toprint[:-1]        #Removes the last character, which is an extra comma, from the string.
    toprint+="}"
    print("") #empty line
    print("Your set is %s." % (Print(A)))   #prints the inputted set
    print("The power set is %s. \n" % (toprint)) #prints the power set and a new line




#Question 3:

##Some text to introduce the program before the menu options; they are placed here rather than below so that they do not keep appearing every time we loop
print("Hello! Welcome to my Power Set Calculator!")
print("This program will allow you to find the power set of a set of your choice.")
print("Let's begin! Please have some fun trying to crash my program. I'm up for the challenge :). \n") #linebreak after text
               

"""This function is a menu. I have seen the light, and simplified my menu function :) """
while True:                 #always commences
    print("Here are your menu options. Please enter a value if you would like to: ")
    print("1. Find the power set of a set. ")
    print("2. Exit. ")
    choice = input("Please choose an option now. ")
    if choice== "1":
        A=Create()          #create your set
        PrintPower(A)       #print the power set in correct math notation
    elif choice=="2":
        print("")
        print("Have a nice day. Goodbye!")
        break               #breaks the if, returns to the upper level of the while loop and therefore starts the menu again
    else:                   #robust menu: all other inputs return the following:
        print("You did not follow the instructions. Please try again. \n")



