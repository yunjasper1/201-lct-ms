"""Jasper Yun, Student ID: 1731131
Exercise 1: Guessing Game
This program hopes to entertain you with a simple guessing game; either the program guesses your number, or you guess a number the program is thinking of."""
from random import randint

#Question 1:

"""This function lets you guess the chosen number. It takes an input of any integer which is defined by "chosen";
later in the program, we use randint(0,100) to randomly choose an integer between 0 and 100 for "chosen".
It returns a response telling the user if the input is a correct guess, or if it is too high or too low."""

def UserGuesses(chosen):
    Correct=False
    while (Correct==False): ##As long as the user does not guess the computer's chosen answer, Correct will remain as False and keep the loop going. 
        UserGuess = int(input("What is your guess? "))
        if (UserGuess == chosen):
            Correct=True
            print("") #To make a blank line
            print("Congratulations on a correct guess!")
        if (UserGuess < chosen):
            Correct=False
            print("Your guess is too low. Try again.")
        if (UserGuess > chosen):
            Correct=False
            print("Your guess is too high. Try again.")


#Question 2:

"""This function guesses the number you have chosen.
The function's input, "min" and "max", are  integers that represent the upper and lower boundaries for the interval of integers that the program needs to guess from;
in our game (question 3), "min" and "max" are 0 and 100, respectively.
The user inputs 1, 2, 3, to represent if the computer guesses correctly, too low, or too high, respectively. It outputs another guess in response to the input."""

def ComputerGuesses(min,max):
    Correct=False
    while (Correct==False): ##As long as the computer does not guess the user's chosen answer, Correct will remain as False and keep the loop going.
        ComputerGuess = int((min+max)/2) ##The optimal guessing formula
        print(ComputerGuess)
        response = int(input("Enter 1 if my guess is your number; 2 if my guess is too small; 3 if my guess is too big. "))
        if (response==1):
            Correct=True
            print("") #For a blank line
            print("Yayy! I did it!")
        if (response==2):
            Correct=False
            min=ComputerGuess
        if (response==3):
            Correct=False
            max=ComputerGuess



#Question 3:

"""This function is the full game, combining the two functions above.
I defined "response" to allow the user to choose which game they want to play, and "response" is given an integer value based on the user's input.
The user inputs 1 or 2 to commence a certain guessing game, either the first or second function from Questions 1 and 2. At the end, you have an option to play again, or to exit the program."""

def FullGuessGame():
    response = int(input("Enter 1 if you want to guess the number I am thinking of. Enter 2 if you want me to guess the number you are thinking of. "))
    if (response == 1): ##A user input of 1 leads to the initiation of the UserGuesses function.
        print("Let's play!")
        print("The number I am thinking of is between 0 and 100.")
        UserGuesses(randint(0,100))
    if (response == 2): ##A user input of 2 leads to the initiation of the ComputerGuesses function.
        print("I'm going to try to guess what number you're thinking of!")
        print("Please think of a number between 0 and 100. Don't tell me though.")
        print("")
        print("Ready? Let's play! My first guess is: ")
        ComputerGuesses(0,100) ##Specifies that min=0, max=100
        print("")
    print("Do you want to play again?")
    ans = int(input("Enter 1 to choose a game. Enter 2 if you want to exit."))
    if (ans==1):
        FullGuessGame() #Restarts the guessing game
    if (ans==2):
        print("")
        print("Thanks for playing. Goodbye!")


print("Let's play a game!")
FullGuessGame() #In order to run the game
