"""Jasper Yun, Student ID: 1731131
Exercise 4: Primes

The goal of this program is to create a calculator that:
    -determines whether an inputted integer is prime
    -generates a list of prime numbers less than a given integer
    -finds the prime factorization of an integer
"""

import math #so that I can use the square root for the optimal value of k to stop at in the first question

#Question 1:

##from assignment 2##


def quotient(a,d):
    """This function takes an input of two integers, a and d, and divides them to find the
        quotient and remainder. The division is performed by subtracting d from a until a < d. If a < 0,
        then the division uses a + d until a > 0.
        The output of the function is the quotient and remainder of the division given by q, r."""
    
    r=a
    q=0
    while (r>=d): #if r >= d, we can keep subtracting r - d until r < d, at which point we will be finished the division
        r=r-d
        q=q+1
    while (r<0):  #if r < 0, then we need to keep adding r + d until r > 0, at which point we will necessarily have 0 <= r < d, and we will be done dividing
        r=r+d
        q=q-1
    return q,r 

def Prime(a):
    """This function takes an input of any integer, a.
        It returns either True or False, indicating whether the inputted integer is a prime number. """
    
    if a>1: #there are no primes less than or equal to 1
        for k in range(2,int(math.sqrt(a)+1)): #the for loop takes integers k from 2, 3, ..., sqrt(a) and divides a by each value of k to see if k is a factor of a
            q,r=quotient(a,k)
            if r==0:
                return False    #the loop breaks if there is a value of k that divides a (r=0), showing that a is not prime
        return True             #the loop ends if there are no values of k for which k divides a
    return False                #there are no primes less than or equal to 1


#Question 2:

def ListPrime(M):
    """This function takes an input of any integer M and returns a list of primes less than M.
        If M<=1, it will return False since there are no primes less than or equal to 1. """
    
    if M>1:                             #there are no primes less than or equal to 1
        ListPrime=[]                    #generates empty list of primes
        for a in range(2,M):            #this for loop takes integer values of a in the interval [2, M) and passes them through the Prime(a) function defined above
            Prime(a)
            if Prime(a)==True: 
                ListPrime.append(a)     #adds "a" to the list of primes if a is prime based on the output of Prime(a)
        return ListPrime                #after all the values of a in range(2,M) are put through Prime(a), return the list of primes generated, i.e. ListPrime
    return False                        #in the menu we would say M<=1 has no primes before it, which takes care of negative integer inputs in the menu

#Question 3:

def Factor(n, primes):
    """This function takes an input of an integer n and a list of primes called primes.
        It will return a list of all the prime factors of the inputted n."""
    
    Factors = []                        #creates an empty list of factors
    if n==1:
        return Factors                  #base case: if n=1 then there are no more prime factors
    else:
        for p in primes:                #this for loop takes all the prime numbers, p, in the list "primes" (generated in the menu) and determines whether they divide n
            if n % p == 0:              #p divides n
                Factors = Factor(int(n/p), primes) #recursive step
                Factors.insert(0,p)     #inserting the prime factor into the list of Factors
                return Factors


#Question 4:

##from exercise 2, the loop function##

def Loop():
    """This function is used to loop back the menu options. It technically does not have any parameters.
        It provides the user with an option to return to the menu or exit the program based on user input.
        Entering 1 calls the Menu() function. Inputting 2 exits the program."""
    
    print("")
    print("Here are two more options for you now:")
    print("1. Return to the menu.")
    print("2. Exit the program.")
    A = (input("Please choose an option. (1 or 2) "))
    while not ((A=="1") or (A=="2")): #To prevent an input that is not 1 or 2; robust menu
        A = (input("Please choose an option by entering 1 or 2. "))
    ans=int(A)  #convert A to integer
    if (ans == 1):
        Menu()  #runs Menu function
    if (ans == 2):
        print("")
        print("Have a nice day! Goodbye!") #exits the program



def Menu():
    """This function is a menu to choose different methods to factor an integer.
It does not have any parameters, but to continue the program the user needs to enter a value for "choice", either 1, 2, 3, or 4.
Choosing a value for "choice" will commence one of the above functions, allowing the user to factor the integer."""


    print("") #Some blank space
    print("Here are your menu options. Please enter a value depending on whether you would like to: ")
    print("1. Test whether an integer is prime. ")
    print("2. List all the primes up until some integer. ")
    print("3. Find the prime factorization of some integer. ")
    print("4. Exit the program. ")
    choice = input("Please choose an option now. ")
    while not ((choice=="1") or (choice=="2") or (choice=="3") or (choice=="4")): #To prevent the program from crashing if the input is not 1,2,3, or 4
        choice = input("You must choose an option by inputting 1, 2, 3, or 4. ")
    choice = int(choice) #changes "choice" into an integer since "choice" was a string to deal with non-integer inputs for "choice" and prevent crashes

    if (choice == 1): 
        print("")
        print("Great choice.")
        while True:                 #robust menu: this while loop will always commence and is used to anticipate non-integer inputs
            A = (input("Please choose an integer. "))
            try:
                a=int(A)            #converts A to an integer
                break
            except ValueError:      #if the input was not an integer, this commences to prevent a crash
                print("Hey! That's not an integer! \n")
        Prime(a)
        if Prime(a)==False:         #if the returned value of Prime(a) was False then a was not prime
            print("")
            print("%s is not prime." % (a))
        if Prime(a)==True:          #if the returned value of Prime(a) was True then a was prime
            print("")
            print("%s is prime." % (a))
        Loop() #to allow the user to loop back to the menu or quit

    if (choice == 2):
        print("")
        print("Let's start with choosing the values.")
        Correct=False               #commence the while loop
        while (Correct==False):     #robust menu: this while loop will always commence and is used to anticipate non-integer or non-positive integer inputs
            m = (input("Please choose a positive integer that you want to list the primes up until. "))
            try:
                M=int(m)            #converts m to an integer
                if M<= 1:           #there are no primes less than or equal to 1
                    Correct=False   #keeps the while loop going
                    print("There are no primes less than or equal to 1. \n")
                else:
                    Correct=True    #breaks while loop since M>1 in this case
            except ValueError:      #if the input was not an integer, this commences to prevent a crash
                print("Hey! That's not an integer! \n")
        if ListPrime(M+1)==False:   #we use ListPrime(M+1) in case M itself is prime, then it will be included in the list of primes; (if you somehow outwit the while loop)
            print("")
            print("You have inputted either a negative integer or zero. There are no primes leading up to a negative integer. ")
        else:
            print("")
            print("The prime numbers up to %s are %s." % (M, ListPrime(M+1)))
        Loop()

    if (choice == 3):
        print("")
        Correct=False               #commence the while loop below
        while (Correct==False):     #negatives will cause errors in my program; also, there are no negative primes at our level
            a = (input("Please choose a positive, nonzero integer to factor into its primes. "))
            try:
                A=int(a)
                if A<=1:
                    Correct=False   #continues the while loop if A<=1 since there are no primes less than or equal to 1
                    print("There are no primes less than or equal to 1.")
                    print("Please choose another integer. \n")
                else:
                    Correct=True    #breaks the while loop
            except ValueError:
                print("Hey! That was not an integer. \n")
                Correct=False       #keeps the while loop going
        primes=ListPrime(A+1)       #creates a list of primes up to A+1 so that A is included in the list if A is prime
        Factors=Factor(A, primes)   #Runs the Factor function and makes the returned list equal to "Factors" so that it can be printed in the next step
        print("The prime factorization of %s is %s" % (A, Factors))
        print("Voila! Beautiful, right?")
        Loop()

    if (choice == 4):
        print("")
        print("Have a nice day! Goodbye!")



##Some text to introduce the program before the menu options; they are placed here rather than in the Loop() so that they do not keep appearing every time we loop
print("Hello! Welcome to my Prime Factorization Calculator!")
print("This program will allow you to find the prime factorization of a positive integer of your choice.")
print("Please note that there are no primes less than or equal to 1.")
print("Let's begin! Please have some fun trying to crash my program. I'm up for the challenge :). \n") #linebreak after text
               
Menu() #to run the program

