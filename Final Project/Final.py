"""Claire Kennedy and Jasper Yun
Topics in Advanced Mathematics
Final Project

The goal of this program is to utilize RSA cryptography to encrypt messages and to decrypt received encoded messages.
In order to achieve this, functions which set up the public (n, e) and private (d) keys are used, which can be achieved using user input or randomly.
These keys are then used in functions which encrypt and decrypt messages.
This program also extends the applications of RSA cryptography to create a bitcoin system. Two classes are created, representing people and transactions
between people. The program 'facilitates' bitcoin transactions and can also give the user data on people and transactions.
The program has a menu which allows the user to choose to create people, transactions, encrypt messages, decrypt messages, print information, and check blocks, among other things.


-------------------------------------------------

For your information, the code is separated into sections according to the assignment instructions.
Each section has a header with lots of # symbols.


                      Table of Contents of this Code:
                                                                      line number
1. Setting up RSA  -----------------------------------------------------  61

        -Question 1.1: PickPrimes()  -----------------------------------  66
        -Question 1.2: SetUp()  ----------------------------------------  147
        -Question 1.3: RandomPickPrimes(min,max)  ----------------------  195
        -Question 1.4: SetUpRandom(min,max)  ---------------------------  211

2. Encoding Messages  --------------------------------------------------  234

        -Question 2.1: GetMessage()  -----------------------------------  239
        -Question 2.2: LetterToNumber(Message)  ------------------------  278
        -Question 2.3: Encode(Message,n,e)  ----------------------------  298

3. Decoding Messages  --------------------------------------------------  315

        -Question 3.1: GetMessageNumber()  -----------------------------  320
        -Question 3.2: NumberToLetter(Message)  ------------------------  378
        -Question 3.3: Decode(Message,n,d)  ----------------------------  390

4. Creating "Person" Class  --------------------------------------------  437

        -Question 4.1: __init__(self,id,name,money)  -------------------  451
        -Question 4.2: RandomSetUp(self,min,max)  ----------------------  462
        -Question 4.3: ReSetUpRSA(self,p,q,e)  -------------------------  479
        -Question 4.4: DigitalSignature(self,M)  -----------------------  577
        -Question 4.5: PublishInformation(self) ------------------------  588
        -Question 4.6: PublicKey(self)  --------------------------------  599
        -Question 4.7: PublishPrivateInformation(self) -----------------  611

5. Creating "Transaction" Class  ---------------------------------------  628

        -Question 5.1: __init__(self,id,previous,amount,sender,receiver)  637
        -Question 5.2: CheckThisBlocks(self)  --------------------------  682
        -Question 5.3: CheckAllBlocks(self)  ---------------------------  735
        -Question 5.4: PrintOne(self)  ---------------------------------  756
        -Question 5.5: PrintAll(self)  ---------------------------------  771

6. Core and Menu  ------------------------------------------------------  792                #will need to be changed after we remove garbage code from Transaction

"""

from Help import *


import random       #for randint



#Expanded dictionary to include numbers and special characters on the keyboard:

Letter = {0:' ',1:'A',2:'B',3:'C',4:'D',5:'E',6:'F',7:'G',8:'H',9:'I',10:'J',11:'K',12:'L',13:'M',14:'N',15:'O',16:'P',17:'Q',18:'R',19:'S',20:'T',21:'U',22:'V',23:'W',24:'X',25:'Y',26:'Z',27:"'",28:',',29:".",30:"!",31:'?',32:'(',33:')',34:';',35:':',36:'"',37:'$',38:'%',39:'@',40:'+',41:'=',42:'-',43:'/',44:'#',45:'^',46:'&',47:'*',48:'\\',49:'`',50:'~',51:'{',52:'}',53:'[',54:']',55:'0',56:'1',57:'2',58:'3',59:'4',60:'5',61:'6',62:'7',63:'8',64:'9',65:'_',66:'<',67:'>',68:'|'}
Number = { Letter[x]:x for x in Letter}
for x in Letter:
    Number[Letter[x].lower()]=x

#since we have 68 characters in the dictionary, p,q > 6869 is required


############################################ 1. Setting up RSA ############################################

#Question 1.1:

def PickPrimes():
    """This function takes no inputs. It outputs (p,q) which are two primes of the user's choice."""
    
    correct=True        #enters while loop for robust program
    while (correct==True):  #ensures m is positive and makes m an integer
        m = (input("Please choose a positive minimum value for your primes: "))
        try:
            m=int(m)    #changing input to integer
            if m<=1:
                print("There are no primes less than or equal to 1. Please choose a positive value. \n")
                correct=True        #continues loop to get a prime; there are no primes less than or equal to 1
            else:
                correct=False       #breaks loop if we have a positive value
        except ValueError:
            print("Hey! That was not an integer. \n") #robust program
            correct=True            #continues loop

    correct=True        #enters while loop for robust program
    while (correct==True):  #ensures M is positive and makes M an integer
        M = (input("Please choose a positive maximum value for your primes greater than %s: " % (m)))

        try:
            M=int(M)    #changing input to integer
            if M<=1:
                print("There are no primes less than or equal to 1. Please choose a positive value. \n")
                correct=True        #continues loop to get a prime
            else:
                correct=False       #breaks loop
        except ValueError:
            print("Hey! That was not an integer. \n") #robust program
            correct=True            #continues loop

    if m<M:
        P=ListPrime(m,M) #robust program: switches m, M depending on if m<M or M<m to prevent empty list result from ListPrime function

    elif M<m:
        P=ListPrime(M,m)
    print('')   #better user experience by line-breaking
    print("Here is a list of primes between the values you have given me: %s." % (P))

    while True:             #loop always begins
        s_correct = True    #ensures next while loop commences

        while (s_correct==True):    #robust program: ensuring p is a prime and that p is an integer
            p = (input("Please pick a prime p from the list. "))
            try:
                p=int(p)
                if Prime(p)==False:
                    print("You chose a composite number.")
                    s_correct=True      #continues the loop if p is not prime
                if Prime(p)==True:
                    s_correct=False     #breaks the loop if p is prime
            except ValueError:
                print("That was not an integer. \n")    #in case of non-integer input

        t_correct = True    #ensures next while loop commences

        while (t_correct==True):    #robust program: ensuring q is a prime and that q is an integer
            q = input("Please pick a prime q from the list. ")  #make robust; if p or q is not prime
            try:
                q=int(q)
                if Prime(q)==False:
                    print("You chose a composite number.")
                    t_correct=True      #continues the loop if p is not prime
                if Prime(q)==True:
                    t_correct=False     #breaks the loop if p is prime
            except ValueError:
                print("That was not an integer. \n")    #in case of non-integer input

        if p==q:
            print("The numbers that you picked are equal. Please try again. \n") #we do not want p=1

        else:
            break #breaks the while loop is all conditions are satisfied

    return (p,q)



#Question 1.2:

def SetUp():
    """This function takes no inputs. It returns the modulo n, the power e,
        and d, the inverse modulo phi(n) of this power e."""
    print("Enter 1 if you would like to use random primes.")
    print("Enter 2 if you would like to choose the primes.")
    choice=input("Please choose now (1 or 2).  ")
    
    while not (choice =="1" or choice =="2"): #robust: 02 = 2, but make that true in program
        choice=input("That was not an option. Please try again (1 or 2). ")
        
    if choice=="1":
        (n,e,d,p,q) = SetUpRandom(min, max) #outputs of SetUpRandom function
        print("The random primes chosen are %s and %s. \n" %(p,q))
        
    elif choice=="2":
        (p,q)=PickPrimes()
        while (p<6869 or q<6869):       #to ensure p,q are greater than 6869
            print("You did not choose a sufficiently large p. Try again.")
            (p,q) = PickPrimes()
    
    n = p*q
    phi_n = (p-1)*(q-1)     #since p,q are primes, we know phi(n) = (p-1)*(q-1)

    correct=False
    while (correct==False):
        E = input("Please pick a positive exponent e. ") #doesn't need to be positive, but keep it
        try:
            e = int(E)
            if e<0:     #reduce the exponent to a positive number? is this included in the functions in Help?
                e = e + phi_n
            if RelativelyPrime(phi_n, e) == False:  #checking if phi(n) and e are relatively prime
                print("The exponent you chose is not relatively prime to phi(n) = phi(p*q) = %s. \n" %(phi_n))
                correct=False                       #while loop continues; pick another e
            elif RelativelyPrime(phi_n, e) == True:
                correct=True    #breaks while loop
        except ValueError:  #robust; in case of non-integer input for e
            print("Hey! That was not an integer. \n")
            Correct=False           #keeps the while loop going

    (d,s,t)=Euclidean(e,phi_n)    #s is the inverse of e mod phi(n)
    d=s #making d the inverse
    if d<0:
        d = d + phi_n
    return (n,e,d,p,q)          #return p,q as well for later on use in the menu


#Question 1.3:

def RandomPickPrimes(min,max):
    """This function takes an input of a min and max value. It outputs two distinct prime numbers between the min and max values."""
    
    min=6869        #ensures that p,q are at least 6869 ###### however, this can be set by the user, then we would have to make conditions for min>6869
    max=99999       #this is a reasonably large value ###### however, this can be set by the user
    Primes = ListPrime(min, max)        #generate list of primes
    r = random.randint(0,len(Primes))   #generate random position in list Primes
    p=Primes[r]                         #let p = prime number at the position r in Primes
    Primes.remove(p)                    #ensures q != p
    s = random.randint(0, len(Primes))  #generate another random position in list Primes
    q=Primes[s]                         #let q = prime number at the position s in Primes
    return (p,q)


#Question 1.4:

def SetUpRandom(min,max):
    """This function takes an input of a min and max value. It outputs the modulo n, the power e,
        and d, the inverse modulo phi(n) of this power e.  """
    
    (p,q) = RandomPickPrimes(min,max)   #choosing random primes p,q
    n = p*q
    phi_n = (p-1)*(q-1)                 #since p,q are primes, we know phi(n) = (p-1)*(q-1)
    Primes = ListPrime(3,100)           #generate list of primes between 3 and 100
    correct=False
    while (correct==False):
        t = random.randint(0,len(Primes))   #generate random position in list Primes
        e = Primes[t]                       #let e = prime number at the position t in Primes
        if RelativelyPrime(phi_n, e) == False:  #checking if phi(n) and e are relatively prime
            correct=False                       #while loop continues; pick another e
        elif RelativelyPrime(phi_n, e) == True:
            correct=True #breaks while loop
    (d,s,t)=Euclidean(e,phi_n)    #s is the inverse of e mod phi(n)
    d=s #making d the inverse
    if d<0:
        d = d + phi_n
    return (n,e,d,p,q)  #return the p,q values as well for SetUp() function to use



############################################ 2. Encoding Messages ############################################

#Question 2.1:

def GetMessage():
    """This function takes no input. It outputs a list of letters which contains the message."""

    print("Enter 1 to enter an entire message.")        #giving user the option to enter entire messages instead of letter by letter
    print("Enter 2 to input the the message manually, one letter at a time.")   #letter by letter option as required by assignment instructions
    choice = input("Please choose now (1 or 2).  ")
    while not ((choice=="1") or (choice=="2") ): #To prevent the program from crashing if the input is not 1 or 2
        choice = input("You must choose an option by inputting 1 or 2. ")
    if (choice=="1"):
        print("")
        message = input("Please enter your message:  ") #inputting message by user
        Message = list(message)         #changes inputted message to a list of letters
        if mod(len(Message),2)==1:  #if Message has odd number of letters
            Message.append(" ")     #add blank space to end of Message
    if (choice=="2"):
        print("")
        Message=[]              #empty list Message
        add = True              #commences while loop below
        print("Any combination of multiple letters together at once will end your message.")
        while (add == True):    #ensures we can keep adding letters
            try:
                letter = input("Please enter the next letter of your message: ")
                Message.append(letter)
                if len(letter)>1:
                    add = False     #ends while loop; finishes message
                    Message.pop()   #removes whatever letters (string) were entered together from end of Message
                if letter == "":
                    add = True
                    print("Invalid input. \n")
            except ValueError:
                print("Hey, that isn't a letter!")
                Correct=False
        if mod(len(Message),2)==1:  #if Message has odd number of letters
            Message.append(" ")     #add blank space to end of Message
    return Message


#Question 2.2:

def LetterToNumber(Message):
    """This function takes an input of Message, list of letters.
        It will return the equivalent of the message in numbers."""

    MessageInNumber=[]      #blank list MessageInNumber
    error = False   #to prevent printing of error statement below
    for x in Message:       #for every letter in the list Message, we transform to corresponding number value
        
        try:
            MessageInNumber.append(Number[x])   #transforming letter to number
        except KeyError:
            error = True    #prints error statement
            MessageInNumber.append(65)  #adds underscore to list of characters to decrypt

    if error == True:
        print("The message contains unsupported characters. Each unsupported character will be replaced by an underscore (_).")
    return MessageInNumber


#Question 2.3:

def Encode(Message,n,e):
    """This function takes an input of a list of letters, Message,
        the modulo n, and e the power. It returns the encrypted message as a list of integers."""
    InNumber=LetterToNumber(Message)    #InNumber is the letters of message in number form
    Coded=[]        #empty list Coded
    while (len(InNumber))!=0:
        x=InNumber.pop(0)   #x = next element of InNumber
        y=InNumber.pop(0)   #y = next element of InNumber
        NumberToEncrypt = 100*x + y     #encrypting two letters at once
        encrypted = pow(NumberToEncrypt,e,n)
        Coded.append(encrypted) #add encrypted block of two letters to list Coded
    return Coded




    

############################################ 3. Decoding Messages ############################################

#Question 3.1:

def ConvertList(string):
    """This function takes an input of a string. It converts the string to a list and returns the list.. """
    li = list(string.split(","))    #converts inputted string to list and excludes commas; list entries are all the characters between commas in the list
    return li 

def GetMessageNumber():
    """This function takes no input. It outputs a list of numbers to decrypt given by the user. """
 
    print("Enter 1 to paste an entire message (list of numbers) with square brackets around them.")       #gives user option to copy/paste encrypted messages instead of typing numbers in
    print("Enter 2 to input the numbers of the message manually, one number at a time.")    #as required by assignment instructions
    choice = input("Please choose now (1 or 2).  ")
    while not ((choice=="1") or (choice=="2") ): #To prevent the program from crashing if the input is not 1 or 2
        choice = input("You must choose an option by inputting 1 or 2. ")
    if (choice=="1"):
        correct=False   #for robust program, to commence while loop below
        while (correct==False): #this loop is for robust program
            somelist = input("What is your message to decrypt? ")
            if (somelist==''): #in case of no message or accidentally hitting enter key
                correct=False   #gives user a chance to enter a new message that is not empty
            elif (somelist=='[]'): #in case user enters empty list for message to decrypt
                print("This message is empty. Decrypted, it would contain nothing. Please enter a non-empty message. \n")
                correct=False   #gives user a chance to enter a proper message to decrypt
            elif (somelist[0]=='[' and somelist[-1]==']'):    #for robust program, to ensure we do not remove numbers from the message by thinking that they are brackets
                somelist = somelist[1:]             #removes [ from the inputted string
                todecrypt = somelist[:-1]           #removes ] from the inputted string
                todecrypt = ConvertList(todecrypt)  #converts string to list
                ToDecrypt = []                      #empty list to add numbers into
                for k in todecrypt:
                    try:
                        k = int(k)              #change string characters to integers
                        ToDecrypt.append(k)     #add integers to list
                    except ValueError:
                        print("not all ints")
                correct=True    #breaks the loop
            else:
                print("Invalid input. Please make sure to include square brackets, [ and ], around your list of integers. \n")
                correct=False   #continues the loop

    if (choice=='2'):        
        add = True  #to commence the loop below
        print("Once you are finished entering your integers to decrypt, please input 'end' to finish the entering process.")
        ToDecrypt = []  #creates empty list to fill with numbers to decrypt
        while (add == True):
            coded = input("Please enter the integer you need to decrypt: ")
            if (coded=="end" or coded=="End" or coded=="eNd" or coded=="enD" or coded=="ENd" or coded=="eND" or coded=="EnD" or coded=="END"):  #to end the loop and stop adding values to ToDecrypt
                correct=True
                break   #ends the loop
            try:
                coded = int(coded)  #transform to an integer
                ToDecrypt.append(coded) #add to list of values to decrypt
            except ValueError:
                print("Hey! That wasn't an integer. \n")    #robust program
    return ToDecrypt



#Question 3.2:

def NumberToLetter(Message):
    """This function takes an input of a list of numbers, Message, between 0 and 26.
        It outputs a list of the corresponding letters."""

    MessageInLetters=[]      #blank list MessageInLetters
    for x in Message:       #for every number in the list Message, we transform to corresponding letter
        MessageInLetters.append(Letter[x])   #transforming number to letter
    return MessageInLetters


#Question 3.3:


def RemoveSpace(string):
    """This function takes an input of a string. It outputs a string without any spaces at the beginning or end of the string."""
    if len(string)==0:
        pass                    #if string input is empty, then don't do anything
    elif (string[-1]==' '):     #runs only if the last character in the string is a space
        string = string[:-1]    #removes the space
        string=RemoveSpace(string)     #runs recursively to ensure it runs completely
    elif (string[-1]=='\t'):    #if last character in string is a tab-space
        string = string[:-1]    #remove the tab
        string=RemoveSpace(string)
    elif (string[0]==' '):      #runs only if the first character in the string is a space
        string = string[1:]     #removes the space
        string=RemoveSpace(string)     #recursive to ensure all spaces are removed
    elif (string[0]=='\t'):
        string = string[1:]     #if first character in string is a tab-space
        string=RemoveSpace(string)     #remove tab
    return string               #returns a string without a space at the end


def Decode(Message,n,d):
    """This function takes an input of a list of numbers, Message, n, the modulo, and d, the private key.
        It returns the decrypted message as a list of letters."""

    DecryptedNumbers = []   #generate blank list to add our decrypted integers to
    for k in Message:       #for each integer in the list of numbers, Message, we want to decrypt
        p=pow(k,d,n)        #makes it so that k^d = p = 100x+y = NumberToEncrypt (from Encode(Message,n,e))
        y = mod(p,100)      #y = p mod 100
        x = int((p-y)/100)  #x = (p-y)/100
        x = mod(x,69)       #dictionary has 68 characters in it
        DecryptedNumbers.append(x)  #add x to list of numbers to change to letters
        DecryptedNumbers.append(y)  #add x to list of numbers to change to letters
    DeMessage = NumberToLetter(DecryptedNumbers)    #numbers to letters
    string=""       #create blank string
    for x in DeMessage:
        string=string+x     #for every element in the list, add them together in the string
    string=RemoveSpace(string)
    return string   #return the string        


############################################ 4. Creating a class called "Person" ############################################



People = []      #global variable list of people from class Person

#id = 1           #global ID that keeps track of number of people created

min = 2800
max = 10000


class Person:

    #Question 4.1:
    
    def __init__(self,idnumber,name,balance):
        """This function takes input of self (the person), the ID number of the person, their name, and their beginning balance.
            It creates the user, and sets up RSA using RandomSetUp()."""
        self.id = idnumber       #person's ID will be length of list, i.e. their position in the list
        self.name = name #some string
        self.balance = balance #balance in this person's account
        self.RandomSetUp(min,max)


    #Question 4.2:
        
    def RandomSetUp(self,min,max):
        """This function randomly sets up RSA for a user by randomly picking primes p and q, then calculates n and d, and chooses e. It links this information to the user."""
        pq = RandomPickPrimes(min,max)
        self.p = int(pq[0])  #p is first entry of pq tuple
        self.q = int(pq[1])  #q is second entry of pq tuple
        Primes = ListPrime(3,100)
        position = random.randint(0, len(Primes)-1) #we do len(Primes)-1 to prevent errors; on the off-chance that python chooses position=len(Primes), then self.e = Primes[position] will raise an error
        self.e = Primes[position]
        self.n = self.p*self.q
        self.d = Inverse(self.e,(self.p - 1)*(self.q - 1))

    #def PersonPrint(self):     #diagnostic to check if program is working

     #   return ("%s, %s, %s. " % (self.id, self.name, self.money))

    #Question 4.3:
     
    def ReSetUpRSA(self,p,q,e):
        """This function takes an input of integers p,q,e, and sets up RSA accordingly."""
        self.p = p
        self.q = q
        self.e = e
        p,q=self.CheckIfPrime(p,q)  #checks if p,q are primes > 6869; if not, then user will choose new primes
        p,q=self.CheckIfEqual(p,q)  #ensures p!=q
        p,q,e=self.CheckInverse(p,q,e)  #ensures that n=pq is relatively prime to e
        self.e = e      #reassigns e to the person
        self.n = p*q    #sets n=pq
        self.d = Inverse(e,(p-1)*(q-1))     #finding inverse of d mod phi(n), which will exist because of while loop forcing an invertible e


    #Some functions I defined for ReSetUpRSA(self,p,q,e):
        
    def CheckIfPrime(self,p,q):
        """This function takes an input of two integers p,q. It checks whether they are prime and greater than 6869. It returns p,q such that p,q are prime and greater than 6869."""
        correct=False   #start while loop below
        while correct==False:       #for robust program
            while Prime(p)==False:  #if p is not prime, loop begins
                p = self.PickP()    #pick a new value of p, , guaranteed to be an integer
                while (p<=6869):    #if p<= 6869, loop begins
                    p = self.PickP()    #pick a new p, guaranteed to be an integer
            while Prime(q)==False:  #same as above but for q
                q = self.PickQ()
                while (q<=6869):
                    q = self.PickQ()
            if (Prime(p)==True and Prime(q)==True and p>6869 and q >6869): #condition to break the loop
                correct=True    #break loop
        return p,q

    def CheckIfEqual(self,p,q):
        """This function takes an input of two integers p,q. It ensures that they are not equal. It returns two distinct prime integers p,q such that p,q > 6869. """
        bigCorrect=False #just needed to start the while loop below
        while bigCorrect==False:
            while p==q:     #if p=q, then while loop begins
                correct=False   #start while loop below
                while correct==False: #for robust program
                    try:
                        q = int(input("Please choose a different prime value of q > 6869: "))
                        if q<6869:
                            correct=False
                        correct=True
                    except ValueError:
                        print("That wasn't an integer.")
                        correct=False
            if Prime(q)==False:
                print("The q value that you chose was not prime.\n")
            if (p!=q and Prime(p)==True and Prime(q)==True and p>6869 and q >6869): #condition to break the loop
                break
        return p,q

    def PickP(self):
        """This function takes no input. It returns a prime integer p > 6869."""
        correct=False
        while correct==False:
            try:
                p = int(input("Please choose a prime value of p > 6869: ")) #guarantees p is integer
                correct=True    #breaks loop
            except ValueError:
                print("That wasn't an integer.")
                correct=False   #keeps loop going
        return p

    def PickQ(self):
        """This function takes no input. It returns a prime integer q > 6869."""
        correct=False
        while correct == False:
            try:
                q = int(input("Please choose a prime value of q > 6869: ")) #guarantees q is integer
                correct=True 	#breaks loop
            except ValueError:
                print("That wasn't an integer.")
                correct=False   #keeps loop going
        return q

    def CheckInverse(self,p,q,e):
        """This function takes an input of integers p,q,e. It checks that e is relatively prime to n=pq. It returns three integers p,q,e where e is relatively prime to n=pq."""
        correct=False #run while loop below
        phi_n = int((p-1)*(q-1)) #p,q will be set by the way the code is written in ReSetUpRSA(self,p,q,e), so we can hardcode phi_n, and p,q will necessarily be integers

        while correct==False:         #for robust program
            while RelativelyPrime(e,phi_n)==False: #if e is not relatively prime to phi_n, then while loop starts
                L_correct=False     
                while L_correct==False: #for a robust program
                    try:
                        e = int(input("Please choose a different exponent that is relatively prime to phi(n)=(p-1)(q-1): "))
                        L_correct=True    #breaks the while loop if we get an integer input for e
                    except ValueError:
                        print("That wasn't an integer at all!")
                        L_correct=False   #keeps the loop going
            if RelativelyPrime(e,phi_n)==True:  #condition to break the while loop
                correct=True
        return p,q,e


    #Question 4.4:
    
    def DigitalSignature(self,M):
        """ This function takes an input of the user (self) and M, their message. It then peforms the necessary operations
            using a hash function to produce a unique digital signature that can be utilized to verify messages. """
        self.n = self.p*self.q  #just to make sure in case n is not defined, then we re-define it
        h = hash(M) #calling the built-in hash function for a message M, which is inputted
        s = pow(h, self.d, self.n) #computes, returns h(m)^d mod n as required for digital signature
        return s    #note that s will be reduced mod n

    
    #Question 4.5:
    
    def PublishInformation(self):
    	"""This function just takes an input of the user (self) and returns, in easy to understand print, the profile of the user. """
    	print("Here is the information for %s:" %(self.name))
    	print("    ID: %s." % (self.id)) 
    	print("    Name: %s." % (self.name))
    	print("    Balance: %s." % (self.balance))
    	print("    Public Key (n,e): (%s, %s)." %(self.n, self.e))


    #Question 4.6:
    	
    def PublicKey(self):
    	"""This function just takes an input of the user (self) and returns the public key (n,e) of the user's RSA setup. """
    	self.p = p
    	self.q = q
    	self.n = p*q #creating self.n
    	n = self.n
    	e = self.e
    	return (n,e) #(n,e) is the public key

    
    #Question 4.7:
    
    def PublishPrivateInformation(self):
        """This function just takes an input of the user (self) and returns the private information: p, q, phi(n), and d."""
        self.p 
        self.q
        self.e
        self.phin=(p-1)*(q-1) #creating self.phin
        phin=(p-q)*(q-1)
        (d,s,t)=Euclidean(e,phin)
        self.d=s #to get d, we define phi, call euclidean to get inverse of e
        return("\nHere is the private information for %s: p: %s; q: %s; phi(n): %s; d: %s.\n" %(self.name,self.p, self.q, self.phin, self.d))
 




############################################ 5. Creating a class called "Transaction" ############################################


min = 2800
max = 10000


class Transaction:

    #Question 5.1:
    
    def __init__(self,idnumber,previous,amount, sender, receiver): #id, previous, amount, sender,receiver
        """ This function creates a transaction by taking inputs transaction, ID number, the previous transaction, amount transacted, the sender, and the receiver.
            It uses these inputs to calculate the hash for the current and previous transaction, and assign new balances to complete the transaction."""
        #idtransaction = idtransaction + 1 #increases the class variable by 1 each time a transaction is created/recorded
        if previous == None:
            self.id = 1
        else:
            self.id = previous.id + 1
        self.previous=previous
        self.amount=amount
        self.sender=sender
        self.receiver=receiver
        b1=self.sender.balance
        a=self.amount
        if (b1 < a): 
            global TransactionCounter
            TransactionCounter = TransactionCounter - 1     #negates the effect of code in the menu
            self.valid=False     #to keep track of valid and invalid transactions; invalid transaction
            if previous == None:
                p = 0   #set hash of previous of the first transaction to zero
                self.previousHash = 0 #set hash of previous of the first transaction to zero
            else:
                p = self.previous.hash     #call the previous hash ##maybe errors here then
                self.previousHash = p ############################  NEWLY IMPLEMENTED...IF ERRORS ARISE, IT'S BECAUSE OF THIS HERE
            print("The previous hash is %s."%(p))
            newHashText = str(p) + str(self.amount) + str(self.sender.id) + str(self.receiver.id) #string concatenation as required in the instructions
            c=hash(newHashText)
            print("The current hash is %s."%(c))
            self.hash = c #assigns this new hash to the current transaction so that it is the previous of future transactions
            print("INVALID transaction.\n")

        elif (b1 >= a):
            s=self.sender.balance #identifying sender and receiver's balance
            r=self.receiver.balance
            a=self.amount #identifying amount of transaction; ensure amount is integer during menu
            x=s-a #simple equations that calculate new balances
            self.sender.balance = x #assign new balances to the sender/receiver
            y=r+a
            self.receiver.balance = y #assign new balance
            print("%s (sender) now has %s, and %s (receiver) now has %s." % (sender.name,x,receiver.name, y)) #prints new balances
            if previous == None:
                p = 0   #set hash of previous of the first transaction to zero
                self.previousHash = 0 #set hash of previous of the first transaction to zero
            else:
                p = self.previous.hash     #call the previous hash ##maybe errors here then
                self.previousHash = p ############################  NEWLY IMPLEMENTED...IF ERRORS ARISE, IT'S BECAUSE OF THIS HERE
            print("The previous hash is %s."%(p))
            newHashText = str(p) + str(self.amount) + str(self.sender.id) + str(self.receiver.id) #string concatenation as required in the instructions
            c=hash(newHashText)
            print("The current hash is %s."%(c))
            self.hash = c #assigns this new hash to the current transaction so that it is the previous of future transactions
            print("The transaction is now complete.\n")
            self.valid = True  #valid transaction

            
    #Question 5.2:
            
    def CheckThisBlocks(self):
        """This function takes the input of a transaction, and verifies whether the hash of the previous transaction and the current hash matches with those in the global list. """
##        global ListTransaction
##        name = self.id
##        for x in ListTransaction:
##            if name==x.id:
##                position = ListTransaction.index(x)
##        pTransaction = ListTransaction[position - 1] #make pTransaction the previous transaction to the self object passed into the function
##        p_2 = pTransaction.hash

        if self.previous==None:     #this one is good
            if ((self.previousHash==0) and (self.hash == hash(str(self.previousHash) + str(self.amount) + str(self.sender.id) + str(self.receiver.id)))):
                return True #informations match
            else:
                return False #informations do not match

        else:   #this one is not good!!
            if ((self.previous.hash == self.previousHash) and (self.hash == hash(str(self.previous.hash) + str(self.amount) + str(self.sender.id) + str(self.receiver.id)))):
                return True
            else:
                if (self.previous.hash==self.previousHash):
                    print("Current transaction hash does not match between blocks:")
                    print("    Self hash  --------  %s" %(self.hash))
                    print("    Computed hash  ----  %s" %(hash(str(self.previous.hash) + str(self.amount) + str(self.sender.id) + str(self.receiver.id))))
                    print("")
                else:
                    print("Previous transaction hash does not match between blocks:")
                    print("    Previous hash  ----  %s" %(self.previous.hash))
                    print("    Variable  ---------  %s" %(self.previousHash))
                    print("")
                return False


##        p_1 = self.previous
##        print(type(p_1))
##        if p_1 == None:
##            d = 0
##            self.previous = 0
##            previous.hash = 0
##            print("The previous is %s whose ID is %s." % (self.previous,self.id))    
##        #print("The previous is %s whose ID is %s." % (p_1,p_1.idnumber))
##        previousHash=previous.hash  #need to check if this matches the hash of the previous object in the global list ListTransaction
##        
##        c=self.hash
##        if ((p_1 == p_2) and (self.hash == hash(str(p_2) + str(self.amount) + str(self.sender.id) + str(self.receiver.id)))):
##            #if hash(self)==c and hash(previous)==p:
##            return True
##        else:
##            return False
##
##        #should return true if previoushash matches with hash of last transaction and current hash matches with rest of information ----- should be fine now (jasper)


    #Question 5.3:
            
    def CheckAllBlocks(self): #returns true if current blocks, all blocks check out
        """ This function has an input self (the specific transaction and checks to see if all of the blocks' hashes can be verified.
            It returns whether all blocks check out or if there is an invalid one."""
        global ListTransaction
        blocksGood = True
        for k in ListTransaction:
            if k.CheckThisBlocks() == False:
                print("Transaction number k=%s."%(k.id))
                print("There is a problem with this transaction.")
                print("Found bad block!")
                blocksGood = False
            if k.CheckThisBlocks()==True:
                print("Transaction number k=%s."%(k.id))
                print("Good block!!!!\n\n")
        if blocksGood == False:
            print("There is an invalid block.\n")
        else:
            print("All blocks are good!\n")
            

    #Question 5.4:
            
    def PrintOne(self):
        """ This function has an input self, the specific transaction, and prints and returns the information about the transaction, including the ID number of the transaction,
            the sender and receiver's IDs and names, and the amount sent."""
        a=self.amount
        s=self.sender.name
        s1=self.sender.id
        r=self.receiver.name
        r2=self.receiver.id
        idnumber=self.id
        if self.valid==True:
            print("This transaction's ID number is %s. %s, with ID %s, sent %s, with ID %s, the amount of %s.\n"%(idnumber,s,s1,r,r2,a)) #prints sentence describing whole transaction, includes amount, names, IDs
        if self.valid == False:
            print("This transaction was INVALID:")
            print("This transaction's ID number is %s. %s, with ID %s, attempted to send %s, with ID %s, the amount of %s.\n"%(idnumber,s,s1,r,r2,a)) #prints sentence describing whole transaction, includes amount, names, IDs


    #Question 5.5:
            
    def PrintAll(self):
        """This function prints every transaction in the global list of all transactions. It returns each transaction's ID number,
            the sender and receiver's IDs and names, and the amount sent."""

        ValidT = []     #create list of successful transactions
        InvalidT = []   #list of unsuccessful transactions
        for k in ListTransaction:
            if k.valid==True:
                ValidT.append(k)
            if k.valid == False:
                InvalidT.append(k)
        print("Here are all of the successful transactions:\n")
        for x in ValidT:
            x.PrintOne()
        print("")
        print("")
        print("Here are all of the INVALID transactions:\n")
        for x in InvalidT:
            x.PrintOne()
            
        print("")   #print a blank line to separate individual transactions, for ease of reading
        print("")       #print a blank line after all transactions have been printed
        print("")       #print a blank line after all transactions have been printed









############################################ 6. Core ############################################




TransactionCounter = 0
#idtransaction = 0      
ListTransaction = []      #global variable list of transactions in class Transaction

People = []     #global variable list of people from class Person
PeopleCounter=0     


def NewNameChecker(name):
    """Takes an input of a string. Checks whether this string matches any of the names that are attributes of objects in the list People.
        Outputs a name that doesn't match any of the existing names."""
    global People   #tell python to use the global list People
    nameChecker=False   #just something to make sure we don't get redundant entries of names
    for x in People:    #check that names are different
        if name==x.name:
            nameChecker=True    #we have a redundant name being entered, so we need to prevent that
            print("Someone else has that name already. Please use a different name.\n")
            newname=ChooseName()#choose a new name
            return newname   #return the new name
    if nameChecker==False: #if there are no redundant name entries
        return name
                     #return the original inputted name

def ChooseName():
    """Takes no input. Outputs a name based on user input. """
    name=NameNotAllSpaces()
    #while (name[-1]==' '): #to remove all spaces form the end of names inputted
    name=RemoveSpace(name)    #to remove spaces at the end of names
        
    nameDifferent=NewNameChecker(name)     #run the program to check if the name matches any other inputted names
    return nameDifferent

def NameNotAllSpaces():
    """This function takes no input. It outputs a string (name) that is neither empty nor all empty spaces. """
    correct=False   #commence loop below to choose a name
    while correct==False:
        name = input("What is your name? ")
        if len(name)==0:    #prevent empty names
            print("Names cannot be empty.\n")
            correct = False     #keeps name-choosing loop alive
        else:
            if name.isspace() == True: #if name contains only whitespace, then the if commences
                print("Names cannot contain only whitespaces. \n")
                correct = False #keeps name-choosing loop alive
            else:
                correct=True #name is neither empty nor all whitespaces
    return name

def ExistNameChecker(string):
    """This function takes an input of a string.
    It checks if this string matches the name of any of the people created."""
    found = False
    for x in People:
        if string==x.name:
            print("")
            string = x
            found = True
            correct = True
            break
    if found==False:
        print("There is no one with that name. \n")
        correct = False
    return correct,string,found


#just for testing the program, automated creating people

for i in range(0,3):
    i = Person(PeopleCounter,str(i),50)
    PeopleCounter = PeopleCounter + 1
    People.append(i)


#Menu:

print("Welcome to my program.")     #some text to improve user experience
print("Please note that this program is case-sensitive. Bob is not the same as bob.\n")
while True:         #will always run, so our menu will always loop unless the while loop is broken
    print("Here are your menu options: \n")     #creates new line
    print("    1: create a person.")            #spaces are to indent the options
    print("    2: create a transaction between two people.")
    print("    3: set up RSA again for a specific person.")
    print("    4: encrypt a message for a specific person.")
    print("    5: decrypt a message as a specific person.")
    print("    6: print information about one person.")
    print("    7: print information about all people.")
    print("    8: check all the blocks in the block chain.")
    print("    9: leave the program. \n")
    choice = input("Please choose an option now:  ")

    if (choice == '1'): #creating person
        
        name=ChooseName()   #run the program to choose a name
        while True:
            try:
                money=int(input("How much money does %s have? " %(name))) #making it robust
                break   #breaks the while loop if money is an integer
            except ValueError:
                print("Please enter an integer value %s's account balance.\n" %(name)) #continues the loop if non-integer money is entered
        #People.append(name)     #adds name (string) to People list so that PeopleCounter increases
        PeopleCounter = PeopleCounter + 1
        person = Person(PeopleCounter,name,money) #feeds information to create the person using the Person class
        #People.pop()            #removes name (string) from People
        People.append(person)    #adds name (object) to the global list; we can call People directly since we are not inside a function, so no need to use trigger word global
        print('%s is now set up! \n' % (name))   



    elif (choice == '2'): #creating transaction
        
        correct=False
        while correct==False:
            sender=input("\nWho is sending the money? ")
            sender=RemoveSpace(sender)  #removes spaces from sender name
            correct,sender,found=ExistNameChecker(sender)     #check if sender name exists already; if not, loops to get an existing sender name

        correct = False
        while correct==False:
            receiver=input("Who is receiving it? ")
            receiver=RemoveSpace(receiver)  #removes spaces from receiver name
            correct,receiver,found=ExistNameChecker(receiver) #checks if receiver name exists already; if not, loops to get an existing receiver name

        correct = False    
        while correct == False: #for robust program, ensure amount is an integer
            try:
                amount=int(input("How much is %s sending? You can only send integer and nonzero positive amounts! " %(sender.name)))
                if (amount<=0):
                    print("You can only send nonzero positive amounts!\n")
                else:
                    correct=True    #breaks loop
            except ValueError:  #robust program
                print("The amount must be an integer. \n")
                correct=False   #keeps loop going
        if amount > sender.balance:
            print("There is not enough money in %s's account to complete the transaction."%(sender.name))
            pass
        if len(ListTransaction)==0: #to prevent errors; if Transaction is an empty list then set previous to be NoneType
            previous = None
        else:
            previous = ListTransaction[len(ListTransaction)-1]  #set transaction to be the last object in Transaction (global list of Transactions)
        TransactionCounter = TransactionCounter + 1     
        transaction = Transaction(TransactionCounter,previous,amount,sender,receiver)
        ListTransaction.append(transaction) #add the new transaction to the list of transactions



    elif (choice == '3'): #re-setup RSA for a specific person
        
        person = input("Who would you like to re-setup RSA for? ")
        found=False
        for x in People:
            if person==x.name:
                print("")       #blank line for user experience
                found = True    #so that the statement below isn't printed
                print("To automatically set up RSA, enter 1.")
                print("To manually set up RSA, enter 2.")
                choice=input("Please choose now (1 or 2). ")
                while not (choice=='1' or choice=='2'): #to make it robust
                    choice=input("Please choose either 1 or 2. ")
                if choice=='1':
                    print("")   #blank line for user experience
                    x.RandomSetUp(min,max) #min and max were hardcoded globally
                    print("RSA has been set up successfully. Have fun encrypting! \n")
                elif choice=='2':
                    print("")   #blank line for user experience
                    print("If you have suitable values for p,q,e, then enter 1.")
                    print("If you need help choosing values for p,q,e, then enter 2.")
                    secondchoice=input("Please choose now (1 or 2). ")
                    while not (secondchoice=='1' or secondchoice=='2'):
                        secondchoice=input("Please choose either 1 or 2. ")
                    if secondchoice=='1':
                        print("")   #blank line
                        print("For your reference, here is a list of primes between 8000 and 10000." ) #only use from 8000 to 10000 for brevity in the command shell
                        Primes=ListPrime(8000,10000) #in case the user wants some primes to use
                        print(Primes, "\n") #prints list of primes, and a blank line
                        while True: #always starts the loop below
                            try:
                                p=int(input("What is your prime value p > 6869? "))
                                break
                            except ValueError: #robust program
                                print("That wasn't an integer.\n")
                        while True: #always starts the loop below
                            try:
                                q=int(input("What is your prime value q > 6869? "))
                                break
                            except ValueError:
                                print("That wasn't an integer.\n")
                        while True: #always starts the loop below
                            try:
                                e=int(input("What is your exponent e? "))
                                break
                            except ValueError:
                                print("That wasn't an integer.\n")
                        x.ReSetUpRSA(p,q,e)
                        print("RSA has been set up successfully. Have fun encrypting! \n")

                    if secondchoice=='2':
                        print("") #blank line for user experience
                        n,e,d,p,q = SetUp()
                        x.n = n
                        x.e = e
                        x.d = d
                        print("RSA has been set up successfully. Have fun encrypting! \n")
                break

        if found==False: #if no one has the name that is entered, then print the following statement
            print("There is no one with that name. \n")

                

    elif (choice == '4'):   #encrypting a message for a specific person
        correct = False     #commence while loop to get user's name
        while (correct == False):
            sender = input("What is your name? This is required for the signature. ")
##            while (sender[-1]==' '):    #to remove spaces from the end of input, since names with spaces at the end are the same as names without spaces at the end
##                sender = RemoveSpace(sender)
##            senderFind = False
##            for x in People:
##                if sender==x.name:
##                    sender = x  #set sender to be the object whose name matches the sender input
##                    senderFind = True   #prevent error statement printing
##                    correct = True      #break outer while loop
##                    break   #break for loop
##            if senderFind == False:
##                print("There is no one with that name.")
##                correct = False
            sender=RemoveSpace(sender)
            correct,sender,found=ExistNameChecker(sender)

                
        correct = False     #commence while loop below
        while correct == False: #get recipient's name
            recipient = input("\nWho is the recipient of your message? ")
##            while (recipient[-1]==' '):    #to remove spaces from the end of input, since names with spaces at the end are the same as names without spaces at the end
##                recipient = RemoveSpace(recipient)
            recipient=RemoveSpace(recipient)
            correct,recipient,found=ExistNameChecker(recipient)
            print('')

            if found==True:
                Message=GetMessage()
                while len(Message)==0:      #in case of accidentally pressing enter key before inputting message
                    response = input("You are about to send an empty message. Do you want to proceed? (Y or N): ")
                    while not (response=='Y' or response=='N'):
                        response = input("You must choose an option by entering Y or N. ")

                    if response=='Y':
                        break #break the loop and continue with an empty message
                    elif response=='N':
                        print("")   #line break for user experience
                        Message=GetMessage() #allow user to enter a new message
                        print("")   #line break for user experience
                    else:
                        print("Invalid input. Please try again.\n")
                        Message=[]  #ensure while loop continues

                stringMessage=""    #set empty string
                for y in Message:  
                    try:    #we want to check that all the characters in the message are in the dictionary
                        v = Number[y]   #just to see if we can find the value for a key y
                        stringMessage=stringMessage+y     #character is in the dictionary if no error at this point
                    except KeyError:        #occurs if character is not in the dictionary
                        stringMessage = stringMessage+'_'   #add the underscore that replaces unsupported characters in the decoded message

##            found = False       #used to check if the name inputted is indeed someone in the list People
##            for x in People:    #check every object in the list
##                if recipient==x.name: #name matches the name of an object in the list
##                    recipient = x   #set recipient to be the object whose name matches recipient
##                    print("")   #blank line
##                    Message=GetMessage()        #for user to enter their message
##                    while len(Message)==0:      #in case of accidentally pressing enter key before inputting message
##                        response = input("You are about to send an empty message. Do you want to proceed? (Y or N): ")
##                        if response=='Y':
##                            break #break the loop and continue with an empty message
##                        elif response=='N':
##                            print("")   #line break for user experience
##                            Message=GetMessage() #allow user to enter a new message
##                            print("")   #line break for user experience
##                        else:
##                            print("Invalid input. Please try again.\n")
##                            Message=[]  #ensure while loop continues
##
##                    stringMessage=""    #set empty string
##                    for y in Message:  
##                        try:    #we want to check that all the characters in the message are in the dictionary
##                            v = Number[y]   #just to see if we can find the value for a key y
##                            stringMessage=stringMessage+y     #character is in the dictionary if no error at this point
##                        except KeyError:        #occurs if character is not in the dictionary
##                            stringMessage = stringMessage+'_'   #add the underscore that replaces unsupported characters in the decoded message
                        

                stringMessage=RemoveSpace(stringMessage)    #removes spaces from the beginning and end of Message
                stringMessage=stringMessage.upper()         #since decoded message will be all uppercase, and hash(a) != hash(A)
                signature = sender.DigitalSignature(stringMessage)    #digital signature of sender
                Coded = Encode(Message,recipient.n,recipient.e) #encrypts the message using the n and e of the object whose name matches the name inputted by the user
                print("\nYour encrypted message is: \n%s\n" % (Coded))

                print("The message was sent by: %s." %(sender.name))
                print("The digital signature is: %s." %(signature))
                print("The messsage was sent to: %s. \n" %(recipient.name))
                print("\nPlease keep track of the message, the digital signature, and the sender.\n\n")
                found = True    #so that the statement below isn't printed
                correct = True  #breaks outer loop
                break
            if found==False: #if no one has the name that is entered, then print the following statement
                print("There is no one with that name. \n")
                correct=False #continue loop to find someone with the correct name



    elif (choice == '5'): #decrypting a message for a specific person
        verifySig = input("Would you like to verify the digital signature attached to the message? (Y/N) ")
        while not (verifySig=='Y' or verifySig=='N'):
            verifySig = input("Please choose an option by entering Y or N: ")

        if verifySig == 'Y':
            checkerst = False
            while checkerst == False:
                sender = input("Who sent the message? ")
                while (sender[-1]==' '):
                    sender = RemoveSpace(sender)

                senderFind = False
                for z in People:
                    if sender == z.name:
                        sender = z
                        senderFind = True
                        checkerst = True    #breaks while loop
                        break   #breaks for loop
                if senderFind == False:
                    print("There is no one with that name.\n") #idk why this is printing even when the top code works
                    checkerst = False
        elif verifySig == 'N':
            print("Alright. We shan't verify the signature.")
        
        correct = False
        while correct == False: 
            recipient_name = input("\nWhat is your name? ")
            while (recipient_name[-1]==' '):    #to remove spaces from the end of input, since names with spaces at the end are the same as names without spaces at the end
                recipient_name = RemoveSpace(recipient_name)
            recipientFind = False
            for x in People:
                if recipient_name==x.name:
                    recipient = x  #set sender to be the object whose name matches the sender input
                    recipientFind = True   #prevent error statement printing
                    correct = True      #break outer while loop
                    break   #break for loop
            if recipientFind == False:
                print("There is no one with that name.\n")
                correct = False
        
        print("\nThis is a cryptography program. Please verify your identity.")
        correct=False
        while correct==False: #use to ensure that the password is an integer
            try:
                password = int(input("Please enter %s's private key. " %(recipient.name)))
                correct = True  #breaks loop if successfully convert password to integer
            except ValueError:
                print("The password must be an integer. Try again. \n")
                correct=False   #continues loop

        found = False #used to check if the name inputted is indeed someone in the list People
        for x in People: #check every object in the list
            if ((recipient_name==x.name and password==x.d) or (recipient_name==x.name and password==0)): #name matches the name of an object in the list
                print("\nVerification complete.\n")
                
                print("")
                Message=GetMessageNumber()    #for user to enter their message
                Decoded = Decode(Message,x.n,x.d) #encrypts the message using the n and d of the object whose name matches the name inputted by the user
#                    print("\nNOTE: An unsupported character in the message is replaced with an underscore (_).")
                if len(Decoded)==0: #in case of an empty message
                    print("The message was empty. That is, there is no message for you.\n")
                else:
                    print("\nYour decrypted message is: %s.\n" % (Decoded)) #Decoded is a string with no spaces at the end

                if verifySig == 'Y':
                    
                    correct = False
                    while correct == False:
                        try:
                            signature = int(input("What was the digital signature attached to the message? "))
                            correct = True
                        except ValueError:
                            print("The signature must be an integer.\n")
                            correct = False

                    h = hash(Decoded)   #this is the H(M) step in our notes
                    h_red = mod(h,sender.n) #reduces the hash mod n of the sender
                    Dsig = pow(signature,sender.e,sender.n)
                    if h_red==Dsig:
                        print("\nThe message was genuinely sent by %s. Yay! Message verified.\n" % (sender.name))
                    else:
                        print("\nThis message was not sent by %s. Beware of scammers...\n" %(sender.name))
                
                print("\n\nNext time, if you are tight on time, enter 0 for the password to bypass that step. :)\n")
                found = True    #so that the statement below isn't printed
                correct = True  #breaks outside while loop
                break

            elif (recipient.name==x.name and password!=x.d):
                print("")
                print("Incorrect password. You will be taken back to the menu.\n")
                found=True #we found the person, but the incorrect password was entered

        if found==False: #if no one has the name that is entered, then print the following statement
            print("There is no one with that name. \n")



    elif (choice == '6'): #printing specific person's public information
        
        correct = False
        while correct == False:
            person=input("\nWhose information do you want? Please enter the name. ") #asks user for person's name
            person=RemoveSpace(person)
            correct,person,found=ExistNameChecker(person)
        if found==True:
            print('')
            person.PublishInformation()
            print('')
##
##        found = False #used to check if the name inputted is indeed someone in the list People
##        for x in People: #check all of the objects in the list
##            if person==x.name: #name matches an object's name in the list
##                print("") #blank line for user experience
##                x.PublishInformation()  #print the object's information
##                print("")   #blank line
##                found = True    #so that the statement below isn't printed
##                break
##        if found==False: #if no one has the name that is entered, then print the following statement
##            print("There is no one with that name. \n")



    elif (choice == '7'): #for this one, chose to make loop that just publishes info for every person (every k) in list of people // publishes information of all people
        
        print("")         #line break
        answer = input("In addition to their public information, would you like to view the transactions that the users are involved in? (Y/N) ")
        while not (answer == 'Y' or answer == 'N'):
            answer = input("Please choose an option by enter Y or N: ")
        if answer == 'Y':
            print('')
            for k in People:  #for every object in the list of People, we will print their information
                k.PublishInformation() #print the objects' information
                matches = False
                SenderList = []     #make empty list of transactions where k is sender
                ReceiverList = []   #empty list where k is receiver

                print("\nHere are the transactions that %s was involved in:\n" % (k.name))
                for v in ListTransaction:
                    if (k == v.sender):
                        matches = True
                        SenderList.append(v)    #add v to the list of transactions where k was sender
                    if (k == v.receiver):
                        matches = True
                        ReceiverList.append(v)  #add v to the list of transactions where k was receiver

                if matches == False:
                    print("%s has not partaken in any transactions yet." %(k.name))
                if matches == True:
                    if len(SenderList) != 0:
                        print("%s as the sender:\n" %(k.name))
                        for s in SenderList:
                            s.PrintOne()
                        print("")
                    if len(ReceiverList) != 0:
                        print("%s as the receiver: \n" %(k.name))
                        for r in ReceiverList:
                            r.PrintOne()
                        print("")
                        print("")
                print("")   #provide line break between printed information for objects
            print("")       #print extra line break after the last object's info has been printed

        if answer == 'N':
            print('')
            for k in People:  #for every object in the list of People, we will print their information
                k.PublishInformation() #print the objects' information
                print('')
            print('')
            print('')


    elif (choice == '8'): #checking all the blocks
        
        if len(ListTransaction)==0:     #if there are no transactions completed yet
            print("There are no transactions in the blockchain.\n")
        else:
            t = ListTransaction[0]
            t.CheckAllBlocks()



    elif (choice == '9'): #exiting program
        print("")       #new line
        print("Have a nice day. Goodbye! \n")
        break           #ends program by breaking the while loop



    elif (choice=='11'): #for testing only
        t = ListTransaction[0]
        if t.CheckThisBlocks() == False:
            print("Your function is wrong")
        else:
            print("yay!!")



    elif (choice=='12'): #for testing only
        person = input("\nWhose private key do you want? ")
        found = False
        while (person[-1]==' '):    #to remove spaces from the end of input, since names with spaces at the end are the same as names without spaces at the end
            person = RemoveSpace(person)
        for x in People:
            if  person==x.name:     #find a matching name
                found = True
                print("%s's private key is %s.\n" %(person, x.d))
        if found==False:
            print("There is no one with that name.\n")

    elif (choice=='13'): #for testing only
        for k in ListTransaction:
            print("Transaction %s:" %(k.id))
            if k.previous==None:
                print("    Previous Transaction ID ---------  Previous was None.")
                print("    Previous Transaction Hash  ----------  0.")
                print("    What Current thinks is previousHash -  idk")
                print("    Current Hash ------------------------  %s." %(k.hash))
            else:
                print("    Previous Transaction ID  ------------  %s." %(k.previous.id))
                print("    Previous Transaction Hash  ----------  %s." %(k.previous.hash))
                print("    What Current thinks is previousHash -  %s." %(k.previousHash))
                print("    Current Hash  -----------------------  %s." %(k.hash))
        

    else:
        print("That was not one of the options. \n")



### LEFT TO DO ###
        
"""

Last things to do: make robust and check to make sure everything works before submitting
oh also clean up garbage code after making sure what we have is perfect

options of the menu to do:
1  create person ------------- done :)
2  create transaction -------- done :)
3  set up RSA again ---------- done :)
4  encrypt message ----------- done :)
5  decrypt message ----------- done :)
6  print one info ------------ done :)
7  print all info ------------ done :)
8  check all blocks ---------- done :)
9  leave --------------------- done :)

"""
